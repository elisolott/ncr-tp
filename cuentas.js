fetch('https://api.npoint.io/e9cb7acc6095dff4004a')
    .then(response => response.json())
    .then(data => {
        console.log(data);
        data.cuentas.forEach(row => {
            const typeId = row.type;
            let existeTipo = false;

            data.tipos.forEach(tipo => {
                if (tipo.id === typeId) {
                    existeTipo = true;
                }
            })

            if (row.saldo < 0 || !existeTipo) {
                return;
            }

            const tr = document.createElement('tr');

            const typeAccount = document.createElement('td');
            const saldoAccount = document.createElement('td');
            const numberAccount = document.createElement('td');

            let type = {};
            
            data.tipos.forEach(tipo => {
                if (tipo.id === typeId) {
                    type = tipo;
                }
            })

            const typeText = document.createTextNode(type.nombre);
            const saldoText = document.createTextNode(`${type.moneda} ${row.saldo.toFixed(2)}`);
            const numberText = document.createTextNode(`${type.codigo} - ${row.number}`);

            typeAccount.appendChild(typeText);
            saldoAccount.appendChild(saldoText);
            numberAccount.appendChild(numberText);
            
            tr.appendChild(numberAccount);
            tr.appendChild(saldoAccount);
            tr.appendChild(typeAccount);

            saldoAccount.classList.add("alinear-derecha");
            numberAccount.classList.add("alinear-derecha");

            const tableAccount = document.getElementById("table-account");

            tableAccount.appendChild(tr);
        });
    });
